
export default () => {
    const data = localStorage.getItem('album-data')
    // only returning as a promise so that API remains consistent if db changes
    // from local storage to external db
    return Promise.resolve(data && JSON.parse(data))
}
