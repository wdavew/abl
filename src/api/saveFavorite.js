
export default (favorites) => {
    localStorage.setItem('album-data', JSON.stringify(favorites))
    // only returning as a promise so that API remains consistent if db changes
    // from local storage to external db
    return Promise.resolve()
}
