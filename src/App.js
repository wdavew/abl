import React, { Component, useEffect, useState } from 'react';
import './App.css';
import { AlbumContainer } from './album'

export default () => {
  return (
    <div className="App">
      <AlbumContainer />
    </div>
  );
}

