import styled from 'styled-components'
import React, { Fragment } from 'react'

const AlbumWrapper = styled.div`
  width: 225px;
  height: 300px;
  margin: 30px;
  border-radius: 5px;
  padding: 10px;
  background-color: #eee;
  overflow-y: hidden;
  :hover {
    cursor: pointer;
  }
`
const AlbumImg = styled.img`
  width: 90%;
`
const Artist = styled.h2`
  font-size: .85em;
  margin: 5px;
  font-weight: 800;
`
const AlbumName = styled.p`
  margin: 0px;
  font-size: .85em;
`
const Favorite = styled.p`
  margin: 0px;
  font-size: 1.5em;
  color: #9b870c;
`
export default ({
    coverImg, artist,
    name,
    handleClick,
    isFavorite,
}) => (
    <AlbumWrapper onClick={handleClick}>
      <AlbumImg src={coverImg}/>
      <Artist>{artist}</Artist>
      <AlbumName>{name}</AlbumName>
      <Favorite>{ isFavorite && "★" }</Favorite>
    </AlbumWrapper>
)
