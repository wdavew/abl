import React from "react";
import Detail from "./Detail";

import renderer from "react-test-renderer";

test("Renders", () => {
  const comp = renderer.create(
    <Detail coverImg="#" artist="Artist" name="Name" category="Category" rights="Rights" nSongs="nSongs" price="Price" />
  );

  expect(comp.toJSON()).toMatchSnapshot()
})
