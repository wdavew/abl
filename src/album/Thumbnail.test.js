import React from "react";
import Thumbnail from "./Thumbnail";

import renderer from "react-test-renderer";

test("Renders", () => {
  const comp = renderer.create(
    <Thumbnail coverImg="#" artist="Artist" name="Name" />
  );

  expect(comp.toJSON()).toMatchSnapshot()
})

test("Renders with star if favorited", () => {
  const comp = renderer.create(
    <Thumbnail coverImg="#" artist="Artist" name="Name" isFavorite />
  );

  expect(comp.toJSON()).toMatchSnapshot()
})
