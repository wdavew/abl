import React, {
  Fragment,
  Component,
  useEffect,
  useState,
  useCallback
} from "react";
import styled from "styled-components";
import { fetchTopAlbums, loadFavorites, saveFavorite } from "../api";
import selectAlbums from "./selectAlbums";
import Thumbnail from "./Thumbnail";
import Detail from "./Detail";

const Header = styled.div`
  background-color: #008080;
  position: fixed;
  top: 0;
  width: 100%;
  font-weight: 800;
  text-align: left;
  line-height: 50px;
  height: 50px;
`;

const Nav = styled.ul`
  float: right;
  list-style-type: none;
  margin: 0;
  padding: 0;

  li {
    margin-left: 10px;
    font-weight: 300;
    font-size: 1em;
    float: left;
  }
  li:hover {
    cursor: pointer;
  }
`;

const StyledLi = styled.span`
  text-decoration: ${props => props.selected ? null : 'underline'}
  font-size: 1em;
  font-weight: 400
  margin: 5px;
  &:hover {
    cursor: ${props => props.selected ? 'default' : 'pointer'}
  }
`
const AlbumGrid = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: row;
  flex-wrap: wrap;
  width: 80%;
  margin: 100px auto;
`;

export default () => {
  const [favorites, setFavorites] = useState({});
  const [filteredFavorites, setFavFilter] = useState(false);
  const [albumDetail, setDetail] = useState(null);
  const [albums, setAlbums] = useState([]);

  useEffect(() => {
    fetchTopAlbums().then(result => setAlbums(selectAlbums(result)));
    loadFavorites().then(favorites => setFavorites(favorites || {}));
  }, []);

  useEffect(() => {
    saveFavorite(favorites);
  }, [favorites]);


  return (
    <Fragment>
      <Header>
        <span style={{'marginLeft': 10}}>Top Albums</span>
        <Nav>
          <StyledLi selected={!filteredFavorites} onClick={() => setFavFilter(false)}>Top Albums</StyledLi>
          <StyledLi selected={filteredFavorites} onClick={() => setFavFilter(true)}>Favorite Albums</StyledLi>
        </Nav>
        {albumDetail && (
          <Detail
            {...albumDetail}
            isFavorite={favorites[albumDetail.id]}
            handleClose={() => setDetail(null)}
            toggleFavorite={() =>
              setFavorites({
                ...favorites,
                [albumDetail.id]: !favorites[albumDetail.id]
              })
            }
          />
        )}
      </Header>
      <AlbumGrid>
        {albums
         .filter(album => filteredFavorites ? favorites[album.id] : true)
         .map(albumData => (
          <Thumbnail
            {...albumData}
            isFavorite={favorites[albumData.id]}
            handleClick={() => setDetail(albumData)}
          />
        ))}
      </AlbumGrid>
    </Fragment>
  );
};
