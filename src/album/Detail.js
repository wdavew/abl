import styled from "styled-components";
  import React from "react";
import Thumbnail from "./Thumbnail";

const AlbumDetail = styled.div`
  background-color: #bbebeb
  font-size: .85em;
  font-weight: 300;
  display: flex;
  width: 100%;
  align-items: center;
  max-height: 350px;
  tr {
    height: 1px;
    line-height: 25px
    padding: 0px;
    margin: 0px;
  }
  td {
    height: 1px;
    padding: 0px;
    margin: 0px;
  }
  button {
    margin-top: 1px;
    font-size: .75em;
  }
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: left;
`

const Favorite = styled.p`
  margin: 0px;
  line-height: 15px;
  font-size: 1.5em;
  color: #9b870c;
`;

const Rights = styled.p`
  margin-bottom: 5px;
  line-height: 15px;
`;

const Column = styled.div`
  width: ${props => (props.width ? props.width : 225)}px;
  align-items: center;
  min-height: 225px;
  max-height: 300px;
  display: inline-block;
`;

export default ({
  coverImg,
  isFavorite,
  name,
  artist,
  category,
  rights,
  nSongs,
  price,
  handleClose,
  toggleFavorite,
}) => (
  <AlbumDetail>
    <Column>
      <img src={coverImg} />
    </Column>
    <Column width={500}>
      <table>
        <tr>
          <td>Album Name</td>
          <td>{name}</td>
        </tr>
        <tr>
          <td>Artist</td>
          <td>{artist}</td>
        </tr>
        <tr>
          <td>Category</td>
          <td>{category}</td>
        </tr>
        <tr>
          <td>Number of songs</td>
          <td>{nSongs}</td>
        </tr>
        <tr>
          <td>Price</td>
          <td>{price}</td>
        </tr>
      </table>
      <Rights>{rights}</Rights>
      <Row>
        <Favorite>{isFavorite && "★"}</Favorite>
        <button style={{ height: 25 }} onClick={toggleFavorite}>{isFavorite ? "Remove from favorites" : "Add to favorites"}</button>
      </Row>
    </Column>
  <Column style={{alignSelf: "right", flexGrow: 1}}>
    <button style={{ float: "right" }} onClick={handleClose}>
      Close
    </button>
  </Column>
  </AlbumDetail>
);
