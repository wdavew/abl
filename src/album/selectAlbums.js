export default (albumData) => albumData.feed.entry.map(
    item => ({
        name: item["im:name"].label,
        coverImg: item["im:image"].find(img => img.attributes.height === "170").label,
        id: item["id"].attributes["im:id"],
        category: item["category"].attributes["term"],
        artist: item["im:artist"].label,
        rights: item.rights.label,
        nSongs: item["im:itemCount"].label,
        price: item["im:price"].label,
    })
)
